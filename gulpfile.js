var config = require('./gulpconfig.json'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    inject = require('gulp-inject'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    cleanCss = require('gulp-clean-css'),
    handlebars = require('gulp-compile-handlebars'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync').create();

/**
 * Default task to run
 */
var defaultTasks = [
    'js',
    'sass',
    'inject',
    'handlebars',
];

if (config.enable_watch) {
    defaultTasks.push('watch');
}

if (config.enable_browsersync) {
    defaultTasks.push('browsersync');
}

/**
 * Run browser-sync to refresh page on changes
 */
gulp.task('browsersync', function () {
    browserSync.init({
        proxy: config.app_url,
    });
    gulp.watch('public/index.html').on('change', browserSync.reload);
});

/**
 * Compile JS files
 */
gulp.task('js', function () {
    gulp.src('src/js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('ama-components.min.js'))
        .pipe(uglify().on('error', gutil.log))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/js'));
});

/**
 * Comile Sass Files
 */
gulp.task('sass', function () {
    gulp.src('src/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCss())
        .pipe(rename({
            suffix: '.min',
        }))
        .pipe(gulp.dest('dist/css'));
});

/**
 * Inject compiled JS and CSS files
 */
gulp.task('inject', function () {
    // gulp.src('src/views/index.handlebars')
    //     .pipe(inject(gulp.src([
    //         'public/js/vendors/**/*.js',
    //         'public/css/vendors/**/*.css',
    //     ], {
    //         read: false,
    //     }), {
    //         name: 'vendors',
    //         ignorePath: '/public',
    //     }))
    //     .pipe(inject(gulp.src([
    //         'public/js/**/*.js',
    //         'public/css/**/*.css',
    //     ], {
    //         read: false,
    //     }), {
    //         ignorePath: '/public',
    //     }))
    //     .pipe(gulp.dest('src/views'));
});

/**
 * Compile Handlebar templates
 */
gulp.task('handlebars', function () {
    // var data = {
    //     app_name: config.app_name,
    //     app_url: config.app_url,
    // };
    //
    // var options = {
    //     ignorePartials: true,
    //     batch: ['src/views/partials']
    // };
    //
    // gulp.src('src/views/index.handlebars')
    //     .pipe(handlebars(data, options))
    //     .pipe(rename('index.html'))
    //     .pipe(gulp.dest('public'));
});

/**
 * Watch for changes
 */
gulp.task('watch', function () {
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/sass/**/*.scss', ['sass']);
    gulp.watch('src/views/**/*.handlebars', ['handlebars']);
    gulp.watch([
            'public/js/**/*.js',
            'public/css/**/*.css',
        ], [
            'inject'
        ]);
});

/**
 * Default tasks
 */
gulp.task('default', [
    'js',
    'sass',
    'inject',
    'handlebars',
    'watch',
    'browsersync',
]);
